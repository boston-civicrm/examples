<?php
/*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.4                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2013                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2013
 * $Id$
 *
 */
class CRM_Contact_Form_Search_Custom_UpcomingBirthdays extends CRM_Contact_Form_Search_Custom_Base implements CRM_Contact_Form_Search_Interface {
  function __construct(&$formValues) {
    parent::__construct($formValues);

    $this->_columns = array(
      ts('Contact Id') => 'contact_id',
      ts('Days until birthday') => 'days_until_birthday',
      ts('Name') => 'display_name',
      ts('Birthday') => 'birthday',
      ts('Age turning') => 'age_turning',
    );
  }

  function buildForm(&$form) {
    $groups = array('' => ts('- select group -')) + CRM_Core_PseudoConstant::allGroup();
    $form->addElement('select', 'group_id', ts('Group'), $groups);

    /**
     * if you are using the standard template, this array tells the template what elements
     * are part of the search criteria
     */
    $form->assign('elements', array('group_id'));
  }

  function all($offset = 0, $rowcount = 0, $sort = NULL,
    $includeContactIDs = FALSE, $justIDs = FALSE
  ) {
    if ($justIDs) {
      $selectClause = "contact_a.id as contact_id";
    }
    else {
    $selectClause = " 

  contact_a.id as contact_id,
  
  coalesce(
    if( datediff(
          birth_date - interval year(birth_date) year, 
          now()      - interval year(now())      year
        ) >= 0 /* in this year */ , 
      datediff(
        birth_date - interval year(birth_date) year, 
        now()      - interval year(now())      year
      ),
      datediff(
        birth_date - interval year(birth_date) year + interval 1 year, 
        now()      - interval year(now())      year
      )
    ),999
  ) as days_until_birthday,
  
  display_name,
  
  coalesce(date_format(birth_date, '%b, %D'),'?') as birthday,
  
  if( datediff(
        birth_date - interval year(birth_date) year, 
        now()      - interval year(now())      year
      ) >= 0 /* in this year */ ,
      year(now()) - year(birth_date),
      year(now()) - year(birth_date) + 1
    ) as age_turning
";
    }
        
    return $this->sql($selectClause,
      $offset, $rowcount, $sort,
      $includeContactIDs, NULL
    );
  }

  function from() {
    return "
from civicrm_contact contact_a
left join civicrm_group_contact gc on gc.contact_id = contact_a.id
left join civicrm_group g on g.id = gc.group_id
";
  }

  function where($includeContactIDs = FALSE) {
    $params = array();

    $count   = 1;
    $clause  = array();
    $groupID = CRM_Utils_Array::value('group_id', $this->_formValues );
    if ($groupID) {
      $params[$count] = array($groupID, 'Integer');
      $clause[] = "gc.group_id = %{$count}";
      $clause[] = "g.is_active";
      $clause[] = "gc.status = 'Added'";      
    }

    if (!empty($clause)) {
      $where = implode(' AND ', $clause);
    }
    else {
      $where = '1'; 
    }

    return $this->whereClause($where, $params);
  }

  function templateFile() {
    return 'CRM/Contact/Form/Search/Custom.tpl';
  }
}

